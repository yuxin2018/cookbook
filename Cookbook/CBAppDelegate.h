//
//  CBAppDelegate.h
//  Cookbook
//
//  Created by yu xin on 12-11-28.
//  Copyright (c) 2012年 yuxin2018@gmail.com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Crashlytics/Crashlytics.h>

@interface CBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
